FROM centos:7

# Install build tools and dependencies
RUN yum groupinstall -y "Development Tools"
RUN yum install -y \
    expat-static \
    gperf \
    libpng-static \
    wget

RUN wget -O - "https://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz" | tar xz
RUN wget -O - "https://download.savannah.gnu.org/releases/freetype/freetype-2.10.4.tar.xz" | tar xJ
RUN wget -O - "https://github.com/harfbuzz/harfbuzz/releases/download/2.7.2/harfbuzz-2.7.2.tar.xz" | tar xJ
RUN wget -O - "https://gitlab.freedesktop.org/fontconfig/fontconfig/-/archive/2.13.92/fontconfig-2.13.92.tar.bz2" | tar xj

ENV PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
ENV CFLAGS="-I/usr/local/include"
ENV LDFLAGS="-L/usr/local/lib"

# Build BZip2
RUN cd bzip2-1.0.8 && \
    make -j $jval libbz2.a && \
    mkdir -p /usr/local/include && \
    mkdir -p /usr/local/lib && \
    cp -a bzlib.h /usr/local/include && \
    cp -a libbz2.a /usr/local/lib;

# Build Freetype without Harfbuzz
RUN cd freetype-2.10.4 && \
    ./configure --prefix=/usr/local --disable-shared --enable-static --with-harfbuzz=no && \
    make -j 13 && \
    make install
 
RUN cd harfbuzz-2.7.2 && \
    ./configure --prefix=/usr/local --disable-shared --enable-static --with-freetype=yes && \
    make -j 13 && \
    make install

RUN cd freetype-2.10.4 && \
    make distclean && \
    ./configure --prefix=/usr/local --disable-shared --enable-static --with-harfbuzz=yes && \
    make -j 13 && \
    make install

RUN cd fontconfig-2.13.92 && \
    FREETYPE_CFLAGS="-I/usr/local/include/freetype2 -I/usr/local/include/harfbuzz -I/usr/include/libpng15 $CFLAGS" FREETYPE_LIBS="-lharfbuzz -lpng -lbz2 -lfreetype" \
        ./autogen.sh --prefix=/usr/local --disable-shared --enable-static && \
    make -j 13 && \
    make install
